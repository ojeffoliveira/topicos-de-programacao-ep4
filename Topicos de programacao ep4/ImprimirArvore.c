//////////////////////////////////////////////////////////////////////////////////
// 
// Autor: @ojeffoliveira 
//
// Use o struct que criei na sua �rvore, se usar um diferente a fun��o 
// n�o vai funcionar ou voc� ter� que atualizar ela.
// 
// Observe que o par�metro da fun��o � um pointer para um pointer para o n� raiz
// 
// Voc� s� precisar chamar a fun��o printTree(), as outras n�o importam.
//
// Entretanto, se achar que as �rvores est�o ficando bagun�adas
// (o que provavelmente ocorrer� se sua tela for bastante pequena) 
// vem de zap
//
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct NoT {
	int valor;
	struct NoT *esquerda;
	struct NoT *direita;
};

typedef struct NoT NoT;

//Essa fun��o retorna a profundidade da �rvore.
//Usada para calcular os espa�os entre cada n� irm�o
//(ou melhor cada n� em um mesmo n�vel)
int getProfundidade(NoT* raiz) {
	int maior = 1, tempEsquerda = 0, tempDireita = 0;

	if (raiz->esquerda == NULL && raiz->direita == NULL)
		return maior;
	if (raiz->esquerda != NULL)
		tempEsquerda = getProfundidade(raiz->esquerda);
	if (raiz->direita != NULL)
		tempDireita = getProfundidade(raiz->direita);

	if (tempEsquerda > tempDireita)
		maior = maior + tempEsquerda;
	else
		maior = maior + tempDireita;

	return maior;
}

//Imprime n espa�os
void Espacos(int n) {
	int i = 0;
	for (; i < n; i++)
		printf(" ");
}

//Fun��o principal. Para imprimir basta chamar ela
void printTree(NoT** raiz) {
	// Profundidade/altura da arvore 
	int profundidade = getProfundidade(*raiz);

	//Espacos � o num de caracteres (espa�os e d�gitos) m�ximo de um n�vel de uma �rvore
	//Ou seja, o �ltimo n�vel ter� esse comprimento, da folha mais a esquerda
	//at� a folha mais a direita.
	//� um n�mero arbitr�rio e talvez a tela do seu computador
	// comporte mais ou menos espa�os
	//Por ex, meu prompt de comando em tela cheia
	//Comporta no m�ximo 160 espa�os/caracteres
	//Coloquei uma potencia de 2 para facilitar	
	int espacos = 128;

	//� o espa�o entre os nos no �ltimo n�vel da �rvore
	int menorVao = (espacos / (pow(2, profundidade)));

	//valores usados para percorrer a �rvore
	//iProf percorre ela na altura: seu valor corresponde ao nivel atual
	//iLargura percorre ela em largura: seu 
	//valor corresponde ao n� atual na largura do n�vel
	int iProf=0, iLargura;

	for (int i = 0; i < espacos; i++)
		printf("_");

	//Isso printa a primeira linha, a da raiz
	{
		printf("\n");
		int temp = pow(2, (profundidade - 1 - iProf))*menorVao;
		Espacos(temp);
		printf("%d", (*raiz)->valor);
		Espacos(temp);
	}

	//Um array de pointers (ainda que aqui s� tenha um) para n�s.
	//� o array com os n�s do nivel anterior ao que ser� impresso
	//a cada itera��o do primeiro for abaixo
	NoT** nivelAnt = raiz;
	for (iProf = 1; iProf < profundidade; iProf++) {
		NoT** nivel = (NoT**)malloc(sizeof(NoT*)*(pow(2, iProf)));
		
		//Este for passa os ponteiros dos n�s do nivel a ser impresso agora
		//Usando os ponteiros dos n�s do nivel acima. 
		int i;
		for (i = 0; i < (pow(2, iProf - 1)); i++) {

			if (nivelAnt[i] == NULL)
				continue;
			if ((nivelAnt[i]->esquerda) != NULL)
				nivel[i * 2] = nivelAnt[i]->esquerda;
			else
				nivel[i * 2] = NULL;
			if ((nivelAnt[i]->direita) != NULL)
				nivel[(i * 2 + 1)] = nivelAnt[i]->direita;
			else
				nivel[(i * 2 + 1)] = NULL;

		}



		//pulando linhas, adicione linhas para aumentar o espa�o
		//entre os n�veis
		printf("\n\n\n\n");
		
		for (iLargura = 0; iLargura < (pow(2, iProf)); iLargura++) {
			//Espa�o anterior ao valor
			int espacoAntesEDepoisNesseNivel = pow(2, (profundidade - 1 - iProf))*menorVao;
			//Espa�o anterior retificado para centralizar o n�mero com base
			//no n�mero de digitos
			if ((nivel[iLargura]) != NULL) {
				if (nivel[iLargura]->valor > 9)
					espacoAntesEDepoisNesseNivel = espacoAntesEDepoisNesseNivel - 1;
				if (nivel[iLargura]->valor > 99)
					espacoAntesEDepoisNesseNivel = espacoAntesEDepoisNesseNivel - 1;
			}
			Espacos(espacoAntesEDepoisNesseNivel);
			
			//valor impresso caso haja um n� 
			if ((nivel[iLargura]) == NULL)
				printf("");
			else
				printf("%d", (nivel[iLargura]->valor));
			
			//espa�o posterior 
			espacoAntesEDepoisNesseNivel = pow(2, (profundidade - 1 - iProf))*menorVao;
			//Espa�o posterior retificado para centralizar o n�mero com base
			//no n�mero de digitos
			if ((nivel[iLargura]) != NULL) {
				espacoAntesEDepoisNesseNivel = espacoAntesEDepoisNesseNivel - 1;

				if (nivel[iLargura]->valor > 999)
					espacoAntesEDepoisNesseNivel = espacoAntesEDepoisNesseNivel - 1;
			}
			
			Espacos(espacoAntesEDepoisNesseNivel);
		}

		nivelAnt = nivel;		
	}
	free(nivelAnt);


	printf("\n\n\n");
}

//Use essa fun��o caso queira aumentar a largura em caracteres na tela da sua 
//�rvore. Ou caso queira diminuir por sua tela ser pequena (e a �rvore sair bagun�ada).
//a fun��o vai printar i caracteres na mesma linha repetidas vezes, i indo de 10 at� n
//Assim voc� poder� ver qual o n�mero m�ximo de caracteres que sua tela comporta.
//Com esse n�mero em m�os voc� pode colocar ele na variavel int espa�os dentro de printTree
//Para conseguir printar �rvores com mais digitos e/ou com mais n�veis.
//Observe que para o EP4 provavelmente n�o precisaremos fazer �rvores t�o grandes que o valor atual de int espa�os
//n�o comporte.
//Entretanto, como estamos lidando com pot�ncias de 2, e n�o podemos imprimir 1.5 espa�os, me parece que n�o faz
void testeEspa�os(int n) {


	for (int i = 50; i <= n; i++) {
		printf("<");
		for (int j = 0; j < i - 2; j++) {
			printf(" ");
		}
		printf(">\n\nEspacos acima:%d\nCaso o '>' tenha ficado na linha debaixo de '<', esse numero de caracteres eh muito grande para sua tela.\n\n", i);
	}

}