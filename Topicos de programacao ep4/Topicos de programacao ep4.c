

#include "pch.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>


//PILHA SEM ORDEM
struct Pilha {
	int *vetor;
	int topo;
};
typedef struct Pilha Pilha;

Pilha* criarPilha(int tamanho);

int estaVazia(Pilha *S);

void push(Pilha *S, int valor);

int pop(Pilha *S);

//INSERÇÂO

	//O próprio push

//EXCLUSÃO

void deletarPilha(Pilha *S, const int valor);

//BUSCA

int buscaPilha(Pilha *S, const int valor);

//MAIOR

int maiorPilha(Pilha* S);

//MENOR

int menorPilha(Pilha* S);

//PILHA CRESCENTE

	//INSERÇÂO

void inserirPilhaCrescente(Pilha *S, const int valor);

//EXCLUSÃO

void deletarPilha(Pilha *S, const int valor);

//BUSCA

int buscaPilhaCrescente(Pilha *S, const int valor);

//MAIOR

int maiorPilhaCrescente(Pilha *S);

//MENOR

int menorPilhaCrescente(Pilha *S);

//PILHA DECRESCENTE

	//INSERÇÂO

void inserirPilhaDecrescente(Pilha *S, const int valor);

//EXCLUSÃO

void deletarPilha(Pilha *S, const int valor);

//BUSCA

int buscaPilhaDecrescente(Pilha *S, const int valor);

//MAIOR

int maiorPilhaDecrescente(Pilha *S);

//MENOR

int menorPilhaDecrescente(Pilha *S);

//FILA SEM ORDEM

struct Fila {
	int *vetor;
	int cabeca;
	int rabo;
	int tamanho;
};

typedef struct Fila Fila;

Fila* criarFila(int tamanho);

void enfileirar(Fila *Q, const int valor);

int desenfileirar(Fila *Q);

//INSERÇÂO

	//O proprio enfileirar

//EXCLUSÃO

void deletarFila(Fila *Q, const int valor);

//BUSCA

int buscarFila(Fila *Q, const int valor);
//MAIOR

int maiorFila(Fila *Q);
//MENOR

int menorFila(Fila *Q);

//FILA CRESCENTE

	//INSERÇÂO


void inserirFilaCrescente(Fila *Q, const int valor);

//EXCLUSÃO

void deletarFila(Fila *Q, const int valor);

//BUSCA

	//retorna -1 caso não haja o valor, se houver retorna a posição 
int buscarFila(Fila *Q, const int valor);

//MAIOR

int maiorFilaCrescente(Fila *Q);

//MENOR

int menorFilaCrescente(Fila *Q);


//FILA DECRESCENTE

	//INSERÇÂO

void inserirFilaDecrescente(Fila *Q, const int valor);

//EXCLUSÃO

void deletarFila(Fila *Q, const int valor);

//BUSCA
//retorna -1 caso não haja o valor, se houver retorna a posição 
int buscarFila(Fila *Q, const int valor);

//MAIOR

int maiorFilaDecrescente(Fila *Q);

//MENOR

int menorFilaDecrescente(Fila *Q);

//LISTA SEM SENTINELA SEM ORDEM

struct Noo {
	int valor;
	struct Noo *prox;
	struct Noo *anterior;
};

typedef struct Noo No;

No* criarNo(const int valor);

//INSERÇÂO

void inserirListaInicio(No **inicio, No *novo);

void inserirListaApos(No *aposEsse, No* novo);

void inserirListaFim(No **inicio, No* novo);

//EXCLUSÃO

void removerNo(No* inicio, No* remover);

//BUSCA

No* buscarValorLista(No* inicio, int valor);

//MAIOR

int maiorLista(No* inicio);
//MENOR

int menorLista(No* inicio);

//LISTA SEM SENTINELA CRESCENTE

	//INSERÇÂO

void inserirListaCrescente(No **inicio, No* novo);

//EXCLUSÃO

void removerNo(No* inicio, No* remover);

//BUSCA

No* buscarValorListaCrescente(No* inicio, int valor);

//MAIOR

int maiorListaCrescente(No* inicio);

//MENOR

int menorListaCrescente(No* inicio);

//LISTA SEM SENTINELA DECRESCENTE

	//INSERÇÂO

void inserirListaDecrescente(No **inicio, No* novo);

//EXCLUSÃO

void removerNo(No* inicio, No* remover);

//BUSCA

No* buscarValorListaDecrescente(No* inicio, int valor);

//MAIOR

int maiorListaDecrescente(No* inicio);

//MENOR

int menorListaDecrescente(No* inicio);

//LISTA COM SENTINELA SEM ORDEM

struct Sentinela {
	No* sentinela;
};
typedef struct Sentinela sentinela;

sentinela* criarSentinela();

//INSERÇÂO

void inserirListaInicioSent(sentinela **inicio, No *novo);

void inserirListaAposSent(No *aposEsse, No* novo);

void inserirListaFimSent(sentinela **inicio, No* novo);
//EXCLUSÃO

void removerNoSent(sentinela **inicio, No* remover);
//BUSCA

No* buscarValorListaSent(sentinela **inicio, int valor);
//MAIOR

int maiorListaSent(sentinela **inicio);
//MENOR

int menorListaSent(sentinela **inicio);

//LISTA COM SENTINELA CRESCENTE

	//INSERÇÂO

void inserirListaCrescenteSent(sentinela **inicio, No* novo);

//EXCLUSÃO

void removerNoSent(sentinela **inicio, No* remover);

//BUSCA

No* buscarValorListaCrescenteSent(sentinela **inicio, int valor);

//MAIOR

int maiorListaCrescenteSent(sentinela **inicio);

//MENOR

int menorListaCrescenteSent(sentinela **inicio);

//LISTA COM SENTINELA DECRESCENTE

	//INSERÇÂO

void inserirListaDecrescenteSent(sentinela **inicio, No* novo);

//EXCLUSÃO

void removerNoSent(sentinela **inicio, No* remover);

//BUSCA

No* buscarValorListaDecrescenteSent(sentinela **inicio, int valor);

//MAIOR

int maiorListaDecrescenteSent(sentinela **inicio);

//MENOR

int menorListaDecrescenteSent(sentinela **inicio);

//ARVORE BALANCEADA 
struct NoT {
	int valor;
	struct NoT *esquerda;
	struct NoT *direita;
};

typedef struct NoT NoT;

NoT* criarNoT(const int valor);
//INSERÇÂO

void inserirNoTree(NoT* raiz, NoT* novo);

//EXCLUSÃO

void deletarTree(NoT** raiz, NoT** deletar);

//BUSCA

NoT* buscarTree(NoT* raiz, int valor);

//MAIOR

int  maiorTree(NoT* raiz);
//MENOR

int  menorTree(NoT* raiz);

//ARVORE DESBALANCEADA 

	//INSERÇÂO

	//Acho que isso não vai requerer uma função específica - 
	//talvez para randomizar o processo apenas	

	//EXCLUSÃO

void deletarTree(NoT** raiz, NoT** deletar);

//BUSCA

//MAIOR

int  maiorTree(NoT* raiz);

//MENOR

int  menorTree(NoT* raiz);

/////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Implementação das funções////////////////////////////////////////

//PILHA SEM ORDEM

Pilha* criarPilha(int tamanho) {
	Pilha* temp = (Pilha *)malloc(sizeof(Pilha));
	temp->topo = -1;
	temp->vetor = (int *)malloc(tamanho * sizeof(int));
	return temp;
}

int estaVazia(Pilha *S) {
	if (S->topo == -1)
		return 1;
	else
		return 0;
}

int pop(Pilha *S) {
	if (estaVazia(S) == 1)
		return -1;
	else {
		S->topo = S->topo - 1;
		return *(S->vetor + S->topo + 1);
	}
}

void push(Pilha *S, int valor) {
	S->topo = S->topo + 1;
	S->vetor[S->topo] = valor;
}

//INSERÇÃO

	//O PUSH

//EXCLUSÃO

void deletarPilha(Pilha *S, const int valor) {
	Pilha* aux = criarPilha(S->topo + 1);
	int i, temp, contador = 0;

	for (i = S->topo; S->topo != -1; i--) {
		temp = pop(S);
		contador++;
		if (temp == valor) {
			break;
		}
		else {
			push(aux, temp);
			contador++;
		}
	}

	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Deletar Pilha sem ordem, n:%d, contador%d\n", S->topo + 2, contador);
}

//BUSCAR
int buscaPilha(Pilha *S, const int valor) {
	Pilha* aux = criarPilha(S->topo + 1);
	int i, retornar, temp, contador = 0;
	retornar = -1;

	for (i = S->topo; S->topo != -1; i--) {
		temp = pop(S);
		contador++;
		push(aux, temp);
		contador++;

		if (temp == valor) {
			retornar = i;
			break;
		}
	}


	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Buscar Pilha sem ordem, n:%d, contador%d\n", S->topo + 1, contador);
	return retornar;
}

// MAIOR ELEMENTO

int maiorPilha(Pilha* S) {
	Pilha* aux = criarPilha(S->topo + 1);
	int maior, temp, contador = 0;
	maior = 0;
	for (; S->topo != -1;) {
		temp = pop(S);
		contador++;
		if (temp > maior)
			maior = temp;
		push(aux, temp);
		contador++;
	}

	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}

	free(aux);
	printf("Maior Pilha sem ordem, n:%d, contador%d\n", S->topo + 1, contador);
	return maior;
}
// MENOR ELEMENTO

int menorPilha(Pilha* S) {
	Pilha* aux = criarPilha(S->topo + 1);
	int i, menor, temp, contador = 0;
	for (i = 0; S->topo != -1; i++) {
		temp = pop(S);
		contador++;
		if (i == 0)
			menor = temp;
		if (temp < menor)
			menor = temp;

		push(aux, temp);
		contador++;
	}

	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Menor Pilha sem ordem, n:%d, contador%d\n", S->topo + 1, contador);
	return menor;
}

//PRINT

void printPilha(Pilha *S) {
	int i;
	for (i = 0; i <= S->topo; i++)
		printf("%d, ", S->vetor[i]);
	printf("\n");
}


//PILHA CRESCENTE

	//INSERIR

void inserirPilhaCrescente(Pilha *S, const int valor) {
	int contador = 0;
	if (S->topo == -1) {
		push(S, valor);
		contador++;
		printf("Inserir pilha crescente, n:%d, contador%d\n", S->topo, contador);
		return;
	}


	Pilha* aux = criarPilha(S->topo + 1);
	int temp;

	for (; S->topo != -1;) {
		temp = pop(S);
		contador++;
		if (temp < valor) {
			push(S, temp);
			contador++;
			push(S, valor);
			contador++;
			break;
		}
		push(aux, temp);
		contador++;
	}
	if (S->topo == -1) {
		push(S, valor);
		contador++;
	}

	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Inserir pilha crescente, n:%d, contador%d\n", S->topo, contador);
}

//EXCLUIR

//BUSCA

int buscaPilhaCrescente(Pilha *S, const int valor) {
	Pilha* aux = criarPilha(S->topo + 1);
	int i, retornar, temp, contador = 0;
	retornar = -1;

	for (i = S->topo; S->topo != -1; i--) {
		temp = pop(S);
		contador++;
		push(aux, temp);
		contador++;
		if (temp == valor) {
			retornar = i;
			break;
		}
		else if (temp < valor) {
			break;
		}
	}


	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Buscar Pilha crescente, n:%d, contador%d\n", S->topo + 1, contador);
	return retornar;
}

//MAIOR
int maiorPilhaCrescente(Pilha *S) {
	int contador = 0;
	int temp = pop(S);
	contador++;
	push(S, temp);
	contador++;
	printf("Maior Pilha crescente, n:%d, contador%d\n", S->topo + 1, contador);
	return temp;
}
//MENOR
int menorPilhaCrescente(Pilha *S) {
	Pilha* aux = criarPilha(S->topo + 1);
	int retornar, temp, contador = 0;

	for (; S->topo != -1;) {
		temp = pop(S);
		contador++;
		push(aux, temp);
		contador++;
	}

	retornar = temp;

	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Menor Pilha crescente, n:%d, contador%d\n", S->topo + 1, contador);
	return retornar;
}


//PILHA DECRESCENTE

	//INSERIR

void inserirPilhaDecrescente(Pilha *S, const int valor) {
	int contador = 0;
	if (S->topo == -1) {
		push(S, valor);
		contador++;
		printf("Inserir Pilha decrescente, n:%d, contador%d\n", S->topo, contador);
		return;
	}


	Pilha* aux = criarPilha(S->topo + 1);
	int temp;

	for (; S->topo != -1;) {
		temp = pop(S);
		contador++;

		if (temp > valor) {
			push(S, temp);
			contador++;
			push(S, valor);
			contador++;
			break;
		}
		push(aux, temp);
		contador++;
	}
	if (S->topo == -1) {
		push(S, valor);
		contador++;
	}

	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}

	printf("Inserir Pilha decrescente, n:%d, contador%d\n", S->topo, contador);
	free(aux);
}
//EXCLUIR

//BUSCA

int buscaPilhaDecrescente(Pilha *S, const int valor) {
	int contador = 0;
	Pilha* aux = criarPilha(S->topo + 1);
	int i, retornar, temp;
	retornar = -1;

	for (i = S->topo; S->topo != -1; i--) {
		temp = pop(S);
		contador++;
		push(aux, temp);
		contador++;
		if (temp == valor) {
			retornar = i;
			break;
		}
		else if (temp > valor) {
			break;
		}
	}


	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Buscar Pilha decrescente, n:%d, contador%d\n", S->topo + 1, contador);
	return retornar;
}

//MAIOR

int maiorPilhaDecrescente(Pilha *S) {
	int contador = 0;
	Pilha* aux = criarPilha(S->topo + 1);
	int retornar, temp;

	for (; S->topo != -1;) {
		temp = pop(S);
		contador++;
		push(aux, temp);
		contador++;
	}

	retornar = temp;

	for (; aux->topo != -1;) {
		push(S, pop(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Maior Pilha decrescente, n:%d, contador%d\n", S->topo + 1, contador);

	return retornar;
}

//MENOR

int menorPilhaDecrescente(Pilha *S) {
	int contador = 0;
	int temp = pop(S);
	contador++;
	push(S, temp);
	contador++;
	printf("Menor Pilha decrescente, n:%d, contador%d\n", S->topo + 1, contador);
	return temp;
}


//FILA CIRCULAR SEM ORDEM

Fila* criarFila(int tamanho) {
	Fila* temp = (Fila*)malloc(sizeof(Fila));
	temp->rabo = 0;
	temp->tamanho = tamanho;
	temp->vetor = (int *)malloc(tamanho * sizeof(int));
	temp->cabeca = -1;
	return temp;
}

int membrosFila(Fila *Q) {
	if (Q->cabeca == -1)
		return 0;
	if (Q->rabo > Q->cabeca) {
		return (Q->rabo - Q->cabeca);
	}
	else {
		return ((Q->rabo) + (Q->tamanho - Q->cabeca));
	}
}
void enfileirar(Fila *Q, const int valor) {
	if (Q->cabeca == Q->rabo)
		return; //Overflow
	Q->vetor[Q->rabo] = valor;
	if (Q->rabo == Q->tamanho - 1)
		Q->rabo = 0;
	else
		Q->rabo = Q->rabo + 1;

	if (Q->cabeca == -1) {
		if (Q->rabo == 0)
			Q->cabeca = Q->tamanho - 1;
		else
			Q->cabeca = Q->rabo - 1;
	}
}

int desenfileirar(Fila *Q) {
	int valor;
	valor = Q->vetor[Q->cabeca];
	if (Q->cabeca == Q->tamanho - 1)
		Q->cabeca = 0;
	else
		Q->cabeca = Q->cabeca + 1;

	if (Q->cabeca == Q->rabo)
		Q->cabeca = -1;

	return valor;
}

// INSERIR 

	//O proprio enfileirar

//DELETAR

void deletarFila(Fila *Q, const int valor) {
	int contador = 0;

	if (Q->cabeca == -1) {
		printf("Excluir Fila sem ordem, n:%d, contador%d\n", membrosFila(Q), contador);
		return;
	}

	Fila *aux = criarFila(Q->tamanho);
	int temp, rabotemp;
	rabotemp = Q->cabeca;

	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;

		if (temp == valor) {
			continue;
		}
		else {
			enfileirar(aux, temp);
			contador++;
		}
	}
	Q->rabo = rabotemp;
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	printf("Excluir Fila sem ordem, n:%d, contador%d\n", membrosFila(Q), contador);
	free(aux);
}

//BUSCAR
	//retorna -1 caso não haja o valor, se houver retorna a posição 
int buscarFila(Fila *Q, const int valor) {
	int contador = 0;

	if (Q->cabeca == -1) {
		return -1;
		printf("Buscar Fila sem ordem, n:%d, contador%d\n", membrosFila(Q), contador);
	}
	Fila *aux = criarFila(Q->tamanho);
	int temp, retornar, rabotemp;
	rabotemp = Q->cabeca;
	retornar = -1;
	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		if (temp == valor) {
			retornar = Q->cabeca - 1;
		}
		enfileirar(aux, temp);
		contador++;
	}
	Q->rabo = rabotemp;
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Buscar Fila sem ordem, n:%d, contador%d\n", membrosFila(Q), contador);
	return retornar;
}

// MAIOR ELEMENTO

int maiorFila(Fila *Q) {
	int contador = 0;

	if (Q->cabeca == -1) {
		printf("Maior Fila sem ordem, n:%d, contador%d\n", membrosFila(Q), contador);
		return -1;
	}
	Fila *aux = criarFila(Q->tamanho);

	int temp, maior, rabotemp;
	rabotemp = Q->cabeca;
	maior = 0;

	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		if (temp > maior) {
			maior = temp;
		}
		enfileirar(aux, temp);
		contador++;
	}
	Q->rabo = rabotemp;
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Maior Fila sem ordem, n:%d, contador%d\n", membrosFila(Q), contador);
	return maior;
}
// MENOR ELEMENTO

int menorFila(Fila *Q) {
	int contador = 0;

	if (Q->cabeca == -1) {
		printf("Menor Fila sem ordem, n:%d, contador%d\n", membrosFila(Q), contador);
		return -1;
	}
	Fila *aux = criarFila(Q->tamanho);

	int temp, menor, rabotemp;
	rabotemp = Q->cabeca;

	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		if (Q->cabeca == rabotemp + 1)
			menor = temp;
		if (temp < menor) {
			menor = temp;
		}
		enfileirar(aux, temp);
		contador++;
	}
	Q->rabo = rabotemp;
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Menor Fila sem ordem, n:%d, contador%d\n", membrosFila(Q), contador);
	return menor;
}
//PRINT

void printFila(Fila *Q) {
	if (Q->cabeca == -1)
		return;
	int i;
	i = Q->cabeca;
	do {
		printf("%d, ", Q->vetor[i]);
		if (i == Q->tamanho - 1)
			i = 0;
		else
			i++;
	} while (i != Q->rabo);

	printf("\n");
}

//FILA CIRCULAR CRESCENTE

	//INSERIR

void inserirFilaCrescente(Fila *Q, const int valor) {
	int contador = 0;

	if (Q->cabeca == -1) {
		enfileirar(Q, valor);
		contador++;
		printf("Inserir Fila crescente, n:%d, contador%d\n", membrosFila(Q), contador);
		return;
	}
	Fila *aux = criarFila(Q->tamanho);
	int temp, rabotemp, inserido;
	rabotemp = Q->cabeca;
	inserido = 0;

	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		if (temp > valor && inserido == 0) {
			enfileirar(aux, valor);
			contador++;
			inserido = 1;
		}
		enfileirar(aux, temp);
		contador++;
	}

	Q->rabo = rabotemp;
	if (inserido == 0) {
		enfileirar(aux, valor);
		contador++;
	}
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Inserir Fila crescente, n:%d, contador%d\n", membrosFila(Q), contador);
}

//EXCLUIR

//BUSCAR

//MAIOR

int maiorFilaCrescente(Fila *Q) {
	int contador = 0;

	if (Q->cabeca == -1) {
		printf("Maior Fila crescente, n:%d, contador%d\n", membrosFila(Q), contador);
		return -1;
	}

	Fila *aux = criarFila(Q->tamanho);
	int temp, retornar, rabotemp;
	rabotemp = Q->cabeca;

	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		enfileirar(aux, temp);
		contador++;
	}
	retornar = temp;

	Q->rabo = rabotemp;
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Maior Fila crescente, n:%d, contador%d\n", membrosFila(Q), contador);
	return retornar;
}
//MENOR

int menorFilaCrescente(Fila *Q) {
	int contador = 0;

	if (Q->cabeca == -1) {
		printf("Menor Fila crescente, n:%d, contador%d\n", membrosFila(Q), contador);
		return -1;
	}

	Fila *aux = criarFila(Q->tamanho);
	int temp, retornar, rabotemp;
	rabotemp = Q->cabeca;

	temp = desenfileirar(Q);
	contador++;
	retornar = temp;
	enfileirar(aux, temp);
	contador++;

	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		enfileirar(aux, temp);
		contador++;
	}

	Q->rabo = rabotemp;
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Menor Fila crescente, n:%d, contador%d\n", membrosFila(Q), contador);

	return retornar;
}
//FILA CIRCULAR DECRESCENTE

	//INSERIR

void inserirFilaDecrescente(Fila *Q, const int valor) {
	int contador = 0;

	if (Q->cabeca == -1) {
		enfileirar(Q, valor);
		contador++;
		printf("Inserir Fila decrescente, n:%d, contador%d\n", membrosFila(Q), contador);
		return;
	}
	Fila *aux = criarFila(Q->tamanho);
	int temp, rabotemp, inserido;
	rabotemp = Q->cabeca;
	inserido = 0;

	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		if (temp < valor && inserido == 0) {
			enfileirar(aux, valor);
			contador++;
			inserido = 1;
		}
		enfileirar(aux, temp);
		contador++;
	}

	Q->rabo = rabotemp;
	if (inserido == 0) {
		enfileirar(aux, valor);
		contador++;
	}
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	printf("Inserir Fila decrescente, n:%d, contador%d\n", membrosFila(Q), contador);
	free(aux);

}

//EXCLUIR

//BUSCAR


//MENOR

int menorFilaDecrescente(Fila *Q) {
	int contador = 0;

	if (Q->cabeca == -1) {
		printf("Menor Fila decrescente, n:%d, contador%d\n", membrosFila(Q), contador);
		return -1;
	}

	Fila *aux = criarFila(Q->tamanho);
	int temp, retornar, rabotemp;
	rabotemp = Q->cabeca;

	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		enfileirar(aux, temp);
		contador++;
	}
	retornar = temp;

	Q->rabo = rabotemp;
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Menor Fila decrescente, n:%d, contador%d\n", membrosFila(Q), contador);
	return retornar;
}

//MAIOR

int maiorFilaDecrescente(Fila *Q) {
	int contador = 0;

	if (Q->cabeca == -1) {
		printf("Maior Fila decrescente, n:%d, contador%d\n", membrosFila(Q), contador);
		return -1;
	}

	Fila *aux = criarFila(Q->tamanho);
	int temp, retornar, rabotemp;
	rabotemp = Q->cabeca;

	temp = desenfileirar(Q);
	contador++;
	retornar = temp;
	enfileirar(aux, temp);
	contador++;
	for (; Q->cabeca != -1;) {
		temp = desenfileirar(Q);
		contador++;
		enfileirar(aux, temp);
		contador++;
	}

	Q->rabo = rabotemp;
	for (; aux->cabeca != -1;) {
		enfileirar(Q, desenfileirar(aux));
		contador++;
		contador++;
	}
	free(aux);
	printf("Maior Fila decrescente, n:%d, contador%d\n", membrosFila(Q), contador);
	return retornar;
}



//LISTA DUPLAMENTE LIGADA CIRCULAR - sem sentinela

int membrosLista(No ** inicio) {
	int contador = 0;
	No* temp = *inicio;
	while ((temp) != NULL) {
		temp = (temp)->prox;
		contador++;
	}
	return contador;
}

No* criarNo(const int valor) {
	No* temp = (No*)malloc(sizeof(No));
	temp->valor = valor;
	temp->prox = NULL;
	temp->anterior = NULL;
	return temp;
}
// INSERIR
void inserirListaInicio(No **inicio, No *novo) {
	if (*inicio == NULL)
		*inicio = novo;
	else {
		novo->prox = *inicio;
		novo->prox->anterior = novo;
		*inicio = novo;
	}
}

void inserirListaApos(No *aposEsse, No* novo) {
	if (aposEsse == NULL) {
		return;
	}

	if ((aposEsse)->prox != NULL) {
		novo->prox = (aposEsse)->prox;
		novo->prox->anterior = novo;
	}
	novo->anterior = aposEsse;
	(aposEsse)->prox = novo;
}

void inserirListaFim(No **inicio, No* novo) {
	int contador = 0;

	if (*inicio == NULL) {
		inserirListaInicio(inicio, novo);
		printf("Inseri Lista sem ordem, n:%d, contador%d\n", membrosLista(inicio) - 1, contador);
		return;
	}
	No* temp = *inicio;
	while ((temp)->prox != NULL) {
		temp = (temp)->prox;
		contador++;
	}
	inserirListaApos(temp, novo);
	printf("Inserir Lista sem ordem, n:%d, contador%d\n", membrosLista(inicio) - 1, contador);
}

//DELETAR

void removerNo(No* inicio, No* remover) {


	if (remover == NULL) {
		printf("Deletar Lista sem ordem- NULL, n:%d, contador%d\n", membrosLista(&inicio), 1);
		return;
	}

	if (remover->anterior != NULL)
		remover->anterior->prox = remover->prox;
	else
		inicio = remover->prox;
	if (remover->prox != NULL)
		remover->prox->anterior = remover->anterior;

	printf("Deletar Lista sem ordem, n:%d, contador%d\n", membrosLista(&inicio) + 1, 1);
	free(remover);
}

//BUSCAR
No* buscarValorLista(No* inicio, int valor) {
	int contador = 0;
	No* temp = inicio;
	while (temp != NULL && temp->valor != valor) {
		temp = temp->prox;
		contador++;
	}

	printf("Buscar Lista sem ordem, n:%d, contador%d\n", membrosLista(&inicio), contador);
	return temp;
}

// MAIOR ELEMENTO
int maiorLista(No* inicio) {
	int contador = 0;

	int maior;
	maior = -1;
	No* temp = inicio;
	while ((temp != NULL)) {
		if ((temp->valor) > maior)
			maior = temp->valor;

		temp = temp->prox;
		contador++;
	}
	printf("Maior Lista sem ordem, n:%d, contador%d\n", membrosLista(&inicio), contador);

	return maior;
}
// MENOR ELEMENTO
int menorLista(No* inicio) {
	int contador = 0;

	int menor;
	menor = -1;
	while (inicio != NULL) {

		if (inicio->anterior == NULL)
			menor = inicio->valor;

		if (inicio->valor < menor)
			menor = inicio->valor;

		inicio = inicio->prox;
		contador++;
	}
	printf("Menor Lista sem ordem, n:%d, contador%d\n", membrosLista(&inicio), contador);
	return menor;
}
//PRINT
void printLista(No* inicio) {
	while (inicio != NULL) {
		printf("%d, ", inicio->valor);
		inicio = inicio->prox;
	}
	printf("\n");
}



//LISTA DUPLAMENTE LIGADA CIRCULAR CRESCENTE - sem sentinela

	//INSERIR

void inserirListaCrescente(No **inicio, No* novo) {
	int contador = 0;

	if (*inicio == NULL || ((*inicio)->valor > novo->valor)) {
		inserirListaInicio(inicio, novo);
		printf("Inserir Lista crescente, n:%d, contador%d\n", membrosLista(inicio) - 1, contador);
		return;
	}
	No* temp = *inicio;
	while ((temp)->prox != NULL &&
		temp->prox->valor < novo->valor) {

		temp = (temp)->prox;
		contador++;
	}
	printf("Inserir Lista crescente, n:%d, contador%d\n", membrosLista(inicio), contador);
	inserirListaApos(temp, novo);
}

//EXCLUSÂO

//Igual a sem ordem

//BUSCAR

No* buscarValorListaCrescente(No* inicio, int valor) {
	int contador = 0;

	while (inicio != NULL && inicio->valor != valor && inicio->valor < valor) {
		inicio = inicio->prox;
		contador++;
	}
	if (inicio != NULL && inicio->valor != valor)
		inicio = NULL;

	printf("Buscar Lista crescente, n:%d, contador%d\n", membrosLista(&inicio), contador);
	return inicio;
}

// MAIOR ELEMENTO
int maiorListaCrescente(No* inicio) {
	int contador = 0;


	if (inicio == NULL) {
		printf("Maior Lista crescente, n:%d, contador%d\n", membrosLista(&inicio), contador);
		return -1;
	}

	while (inicio->prox != NULL) {
		inicio = inicio->prox;
		contador++;
	}

	printf("Maior Lista crescente, n:%d, contador%d\n", membrosLista(&inicio), contador);
	return inicio->valor;
}
// MENOR ELEMENTO
int menorListaCrescente(No* inicio) {
	if (inicio == NULL) {
		printf("Menor Lista crescente, n:%d, contador%d\n", membrosLista(&inicio), 1);
		return -1;
	}

	printf("Menor Lista crescente, n:%d, contador%d\n", membrosLista(&inicio), 1);

	return inicio->valor;
}


//LISTA DUPLAMENTE LIGADA CIRCULAR DECRESCENTE - sem sentinela

	//INSERIR

void inserirListaDecrescente(No **inicio, No* novo) {
	int contador = 0;

	if (*inicio == NULL || (*inicio)->valor < novo->valor) {
		inserirListaInicio(inicio, novo);
		printf("Inserir Lista decrescente, n:%d, contador%d\n", membrosLista(inicio) - 1, contador);
		return;
	}
	No* temp = *inicio;
	while ((temp)->prox != NULL &&
		temp->prox->valor > novo->valor) {
		temp = (temp)->prox;
		contador++;
	}

	inserirListaApos(temp, novo);
	printf("Inserir Lista decrescente, n:%d, contador%d\n", membrosLista(inicio) - 1, contador);
}

//BUSCAR


No* buscarValorListaDecrescente(No* inicio, int valor) {
	int contador = 0;

	while (inicio != NULL && inicio->valor != valor && inicio->valor > valor) {
		inicio = inicio->prox;
		contador++;
	}
	if (inicio != NULL && inicio->valor != valor)
		inicio = NULL;

	printf("Buscar Lista decrescente, n:%d, contador%d\n", membrosLista(&inicio), contador);
	return inicio;
}

// MAIOR ELEMENTO

int maiorListaDecrescente(No* inicio) {
	printf("Maior Lista decrescente, n:%d, contador%d\n", membrosLista(&inicio), 1);

	if (inicio == NULL)
		return -1;

	return inicio->valor;

}

// MENOR ELEMENTO

int menorListaDecrescente(No* inicio) {
	int contador = 0;

	if (inicio == NULL) {
		printf("Menor Lista decrescente, n:%d, contador%d\n", membrosLista(&inicio), contador);
		return -1;
	}
	while (inicio->prox != NULL) {
		inicio = inicio->prox;
		contador++;
	}
	printf("Menor Lista decrescente, n:%d, contador%d\n", membrosLista(&inicio), contador);
	return inicio->valor;
}

//LISTA DUPLAMENTE LIGADA CIRCULAR - com sentinela

	//criarSentinela

sentinela* criarSentinela() {
	sentinela* senti = (sentinela *)malloc(sizeof(sentinela));
	senti->sentinela = criarNo(-2);
	return senti;
}

int membrosListaSent(sentinela **inicio) {
	int contador = 0;
	No* temp = (*inicio)->sentinela->prox;
	while ((temp->valor) != -2) {
		temp = (temp)->prox;
		contador++;
	}
	return contador;
}
//INSERIR

void inserirListaInicioSent(sentinela **inicio, No *novo) {
	if ((*inicio)->sentinela->prox == NULL) {
		(*inicio)->sentinela->prox = novo;
		(*inicio)->sentinela->anterior = novo;
		(*inicio)->sentinela->prox->anterior = (*inicio)->sentinela;
		(*inicio)->sentinela->prox->prox = (*inicio)->sentinela;
	}
	else {
		novo->prox = (*inicio)->sentinela->prox;
		novo->prox->anterior = novo;
		(*inicio)->sentinela->prox = novo;
		(*inicio)->sentinela->prox->anterior = (*inicio)->sentinela;
	}
}

void inserirListaAposSent(No *aposEsse, No* novo) {
	if (aposEsse == NULL) {
		return;
	}

	if (aposEsse->prox == NULL && aposEsse->anterior == NULL) {
		novo->prox = aposEsse;
		aposEsse->prox = novo;
		novo->prox->anterior = novo;
		novo->anterior = aposEsse;
	}

	novo->prox = aposEsse->prox;
	aposEsse->prox = novo;
	novo->prox->anterior = novo;
	novo->anterior = aposEsse;

}

void inserirListaFimSent(sentinela **inicio, No* novo) {

	if ((*inicio)->sentinela->prox == NULL) {
		inserirListaInicioSent(inicio, novo);
		printf("Inseri Lista sem ordem com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), 1);
		return;
	}
	inserirListaApos((*inicio)->sentinela->anterior, novo);
	printf("Inserir Lista sem ordem com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), 1);
}

//DELETAR

void removerNoSent(sentinela **inicio, No* remover) {
	if (remover == NULL || (*inicio)->sentinela->prox == NULL)
		return;

	if (remover->prox == remover->anterior) {
		remover->anterior->prox = NULL;
		remover->anterior->anterior = NULL;
	}
	else {
		remover->prox->anterior = remover->anterior;
		remover->anterior->prox = remover->prox;
	}

	free(remover);

	printf("Remover Lista sem ordem com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), 1);

}

//BUSCAR

No* buscarValorListaSent(sentinela **inicio, int valor) {
	int contador = 0;

	No* temp = (*inicio)->sentinela->prox;
	while (temp != NULL && temp->valor != -2 && temp->valor != valor) {
		temp = temp->prox;
		contador++;
	}
	if (temp->valor == -2)
		temp = NULL;
	printf("Buscar Lista sem ordem com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), contador);

	return temp;
}

// MAIOR ELEMENTO

int maiorListaSent(sentinela **inicio) {
	int contador = 0;

	int maior;
	maior = -1;
	No* temp = (*inicio)->sentinela->prox;

	while (temp != NULL && temp->valor != -2) {
		if (temp->valor > maior)
			maior = temp->valor;

		temp = temp->prox;
		contador++;
	}
	printf("Maior Lista sem ordem com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), contador);

	return maior;
}

// MENOR ELEMENTO

int menorListaSent(sentinela **inicio) {
	int contador = 0;

	int menor;
	menor = -1;
	No* temp = (*inicio)->sentinela->prox;
	while (temp != NULL && temp->valor != -2) {

		if (temp->anterior->valor == -2)
			menor = temp->valor;

		if (temp->valor < menor)
			menor = temp->valor;

		temp = temp->prox;
		contador++;
	}
	printf("Menor Lista sem ordem com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), contador);

	return menor;
}

//PRINT

void printListaSent(sentinela **inicio) {
	No* temp = (*inicio)->sentinela->prox;
	while (temp != NULL && temp->valor != -2) {
		printf("%d, ", temp->valor);
		temp = temp->prox;
	}
	printf(".\n");
}


//LISTA DUPLAMENTE LIGADA CIRCULAR CRESCENTE- com sentinela

//INSERIR

void inserirListaCrescenteSent(sentinela **inicio, No* novo) {
	int contador = 0;

	if ((*inicio)->sentinela->prox == NULL || (*inicio)->sentinela->prox->valor > novo->valor) {
		inserirListaInicioSent(inicio, novo);
		printf("Inserir Lista crescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio) - 1, contador);
		return;
	}
	No* temp = (*inicio)->sentinela->prox;
	while ((temp)->prox->valor != -2 &&
		temp->prox->valor < novo->valor) {

		temp = (temp)->prox;
		contador++;
	}
	inserirListaAposSent(temp, novo);
	printf("Inserir Lista crescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio) - 1, contador);

}

//DELETAR

//Igual a sem ordem

//BUSCAR

No* buscarValorListaCrescenteSent(sentinela **inicio, int valor) {
	int contador = 0;

	No* temp = (*inicio)->sentinela->prox;
	while (temp != NULL && temp->valor != -2 && temp->valor != valor
		&& temp->valor < valor) {
		temp = temp->prox;
		contador++;
	}
	if (temp == NULL || temp->valor == -2 || temp->valor != valor)
		temp = NULL;

	printf("Buscar Lista crescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), contador);
	return temp;
}

//MAIOR ELEMENTO

int maiorListaCrescenteSent(sentinela **inicio) {
	printf("Maior Lista crescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), 1);
	if ((*inicio)->sentinela->prox == NULL)
		return -1;

	return (*inicio)->sentinela->anterior->valor;
}

// MENOR ELEMENTO

int menorListaCrescenteSent(sentinela **inicio) {
	printf("Menor Lista crescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), 1);
	if ((*inicio)->sentinela->prox == NULL)
		return -1;

	return (*inicio)->sentinela->prox->valor;
}


//LISTA DUPLAMENTE LIGADA CIRCULAR CRESCENTE- com sentinela

	//INSERIR

void inserirListaDecrescenteSent(sentinela **inicio, No* novo) {
	int contador = 0;

	if ((*inicio)->sentinela->prox == NULL || (*inicio)->sentinela->prox->valor < novo->valor) {
		inserirListaInicioSent(inicio, novo);
		printf("Inserir Lista decrescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio) - 1, contador);
		return;
	}
	No* temp = (*inicio)->sentinela->prox;
	while ((temp)->prox->valor != -2 &&
		temp->prox->valor > novo->valor) {

		temp = (temp)->prox;
		contador++;
	}
	inserirListaAposSent(temp, novo);
	printf("Inserir Lista decrescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio) - 1, contador);
}

//BUSCAR

No* buscarValorListaDecrescenteSent(sentinela **inicio, int valor) {
	int contador = 0;

	No* temp = (*inicio)->sentinela->prox;
	while (temp != NULL && temp->valor != -2 && temp->valor != valor
		&& temp->valor > valor) {
		temp = temp->prox;
		contador++;
	}
	if (temp == NULL || temp->valor == -2 || temp->valor != valor)
		temp = NULL;

	printf("Buscar Lista decrescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), contador);
	return temp;
}

//MAIOR ELEMENTO

int maiorListaDecrescenteSent(sentinela **inicio) {
	printf("Maior Lista decrescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), 1);
	if ((*inicio)->sentinela->prox == NULL)
		return -1;


	return (*inicio)->sentinela->prox->valor;
}

// MENOR ELEMENTO

int menorListaDecrescenteSent(sentinela **inicio) {

	printf("Menor Lista decrescente com sentinela, n:%d, contador%d\n", membrosListaSent(inicio), 1);

	if ((*inicio)->sentinela->prox == NULL)
		return -1;

	return (*inicio)->sentinela->anterior->valor;
}


//ARVORE BINRIA - BALANCEADA

NoT* criarNoT(const int valor) {
	NoT* temp = (NoT*)malloc(sizeof(NoT));
	temp->valor = valor;
	temp->direita = NULL;
	temp->esquerda = NULL;
	return temp;
}
// INSERIR 

int contarNos(NoT* raiz) {
	int soma;
	soma = 1;
	if (raiz == NULL)
		return 0;
	if ((raiz->esquerda) == NULL && (raiz->direita) == NULL)
		return soma;
	if (raiz->esquerda != NULL)
		soma = soma + contarNos(raiz->esquerda);
	if (raiz->direita != NULL)
		soma = soma + contarNos(raiz->direita);

	return soma;
}

void inserirNoTree(NoT* raiz, NoT* novo) {

	if (raiz->esquerda == NULL && raiz->direita == NULL) {
		raiz->esquerda = novo;
		return;
	}
	if (raiz->esquerda == NULL && raiz->direita != NULL) {
		raiz->esquerda = novo;
		return;
	}
	if (raiz->esquerda != NULL && raiz->direita == NULL) {
		raiz->direita = novo;
		return;
	}

	int NumEsquerda, NumDireita;
	NumEsquerda = contarNos(raiz->esquerda);
	NumDireita = contarNos(raiz->direita);

	printf("Inserir Arvore binaria balanceada, n:%d, contador%d, raiz subarovre:%d\n", contarNos(raiz), 1, raiz->valor);
	if (NumEsquerda > NumDireita)
		inserirNoTree(raiz->direita, novo);
	else
		inserirNoTree(raiz->esquerda, novo);
}


// BUSCAR
NoT* buscarTree(NoT* raiz, int valor) {
	if (raiz->valor == valor)
		return raiz;

	NoT* retornar = NULL;

	if (raiz->esquerda != NULL) {
		printf("Buscar Arvore binaria , n:%d, contador%d, raiz subarovre:%d\n", contarNos(raiz), 1, raiz->valor);
		retornar = buscarTree(raiz->esquerda, valor);
	}

	if ((raiz->direita != NULL && retornar == NULL) ||
		(raiz->direita != NULL && retornar != NULL && retornar->valor != valor)) {
		printf("Buscar Arvore binaria , n:%d, contador%d, raiz subarovre:%d\n", contarNos(raiz), 1, raiz->valor);
		retornar = buscarTree(raiz->direita, valor);
	}

	return retornar;
}

// DELETAR 

NoT** buscarTreePai(NoT** raiz, NoT** filho) {
	NoT** retornar = NULL;

	if (*raiz == NULL || *filho == NULL || ((*raiz) == (*filho)))
		return retornar;

	if ((*raiz)->direita == *filho || (*raiz)->esquerda == *filho)
		return raiz;
	if ((*raiz)->esquerda != NULL) {
		printf("Deletar Arvore binaria , n:%d, contador%d, raiz subarovre:%d\n", contarNos(*raiz), 1, (*raiz)->valor);
		retornar = buscarTreePai(&(*raiz)->esquerda, filho);
	}
	if ((*raiz)->direita != NULL && retornar == NULL) {
		printf("Deletar Arvore binaria , n:%d, contador%d, raiz subarovre:%d\n", contarNos(*raiz), 1, (*raiz)->valor);
		retornar = buscarTreePai(&(*raiz)->direita, filho);
	}

	return retornar;
}

void subirFilho(NoT** pai, NoT** filhoFuturo) {

	if ((*filhoFuturo) == NULL || (filhoFuturo) == NULL)
		return;

	//Casos pai-filho
		//FIlho futuro é uma folha
	if ((*filhoFuturo)->esquerda == NULL && (*filhoFuturo)->direita == NULL) {
		(*filhoFuturo)->esquerda = (*pai)->esquerda;
		return;
	}

	//filho futuro tem  dois filhos
	if ((*filhoFuturo)->esquerda != NULL && (*filhoFuturo)->direita != NULL) {
		subirFilho(filhoFuturo, &((*filhoFuturo)->direita));
		(*filhoFuturo)->esquerda = (*pai)->esquerda;
		return;
	}
	//filho futuro tem um filho na esquerda
	if ((*filhoFuturo)->esquerda != NULL) {
		(*filhoFuturo)->direita = (*filhoFuturo)->esquerda;
		(*filhoFuturo)->esquerda = (*pai)->esquerda;
		return;
	}
	//filho futuro tem um filho na direita 
	else if ((*filhoFuturo)->direita != NULL) {
		(*filhoFuturo)->esquerda = (*pai)->esquerda;
		return;
	}
}

void deletarTree(NoT** raiz, NoT** deletar) {
	if (*deletar == NULL || *raiz == NULL)
		return;


	else {
		//Encontrar pai de deletar 
		NoT** pai = (buscarTreePai(raiz, deletar));

		//Lidar com uma folha 
		if ((*deletar)->direita == NULL && (*deletar)->esquerda == NULL) {
			if (*raiz == *deletar) {
				*raiz = NULL;
				free(*deletar);
				return;
			}

			if ((*pai)->direita == *deletar)
				(*pai)->direita = NULL;

			else {
				(*pai)->esquerda = NULL;
				if ((*pai)->direita != NULL) {
					(*pai)->esquerda = (*pai)->direita;
					(*pai)->direita = NULL;
				}
			}
			free(*deletar);
			return;
		}

		//Lidar com apenas um filho 
		if (((*deletar)->direita != NULL || (*deletar)->esquerda != NULL) &&
			!((*deletar)->direita != NULL && (*deletar)->esquerda != NULL)) {
			if (*raiz == *deletar) {
				if ((*raiz)->direita != NULL)
					*raiz = (*raiz)->direita;
				else
					*raiz = (*raiz)->esquerda;

				free(*deletar);
				return;
			}

			if ((*pai)->direita == *deletar) {
				(*pai)->direita = NULL;
				if ((*deletar)->direita != NULL)
					(*pai)->direita = (*deletar)->direita;
				else
					(*pai)->direita = (*deletar)->esquerda;
			}
			else {
				(*pai)->esquerda = NULL;

				if ((*deletar)->direita != NULL)
					(*pai)->esquerda = (*deletar)->direita;
				else
					(*pai)->esquerda = (*deletar)->esquerda;
			}

			free(*deletar);
			return;
		}

		//Lidar com dois filhos

		subirFilho(deletar, &((*deletar)->direita));
		if (*raiz == *deletar) {
			*raiz = (*raiz)->direita;
			free(*deletar);
			return;
		}

		if (((*pai))->direita != NULL && ((*pai))->direita == *deletar) {
			(*deletar)->direita->esquerda = ((*pai))->direita->esquerda;
			((*pai))->direita = (*deletar)->direita;
			free(*deletar);
			return;
		}
		if (((*pai))->esquerda != NULL && ((*pai))->esquerda == *deletar) {
			(*deletar)->direita->esquerda = ((*pai))->esquerda->esquerda;
			((*pai))->esquerda = (*deletar)->direita;
			free(*deletar);
			return;
		}
	}

}

// MAIOR ELEMENTO

int  maiorTree(NoT* raiz) {

	if (raiz->direita == NULL && raiz->esquerda == NULL)
		return raiz->valor;

	int retornar, esquerda = 0, direita = 0;

	if (raiz->esquerda != NULL) {
		printf("Maior Arvore binaria balanceada, n:%d, contador%d, raiz subarovre:%d\n", contarNos(raiz), 1, raiz->valor);
		esquerda = maiorTree(raiz->esquerda);
	}
	if ((raiz->direita != NULL)) {
		printf("Maior Arvore binaria balanceada, n:%d, contador%d, raiz subarovre:%d\n", contarNos(raiz), 1, raiz->valor);
		direita = maiorTree(raiz->direita);
	}
	if (esquerda > direita)
		retornar = esquerda;
	else
		retornar = direita;

	if (raiz->valor > retornar)
		retornar = raiz->valor;

	return retornar;
}

// MENOR ELEMENTO

int  menorTree(NoT* raiz) {
	if (raiz->direita == NULL && raiz->esquerda == NULL)
		return raiz->valor;

	int retornar, esquerda = 0, direita = 0;

	if (raiz->esquerda != NULL) {
		printf("Menor Arvore binaria balanceada, n:%d, contador%d, raiz subarovre:%d\n", contarNos(raiz), 1, raiz->valor);
		esquerda = menorTree(raiz->esquerda);
	}
	if ((raiz->direita != NULL)) {
		printf("Menor Arvore binaria balanceada, n:%d, contador%d, raiz subarovre:%d\n", contarNos(raiz), 1, raiz->valor);
		direita = menorTree(raiz->direita);
	}
	if (raiz->direita == NULL)
		retornar = esquerda;
	else if (raiz->esquerda == NULL)
		retornar = direita;
	else if (esquerda < direita)
		retornar = esquerda;
	else
		retornar = direita;

	if (raiz->valor < retornar)
		retornar = raiz->valor;

	return retornar;
}	//PRINT

int getProfundidade(NoT* raiz) {
	int maior = 1, tempEsquerda = 0, tempDireita = 0;

	if (raiz->esquerda == NULL && raiz->direita == NULL)
		return maior;
	if (raiz->esquerda != NULL)
		tempEsquerda = getProfundidade(raiz->esquerda);
	if (raiz->direita != NULL)
		tempDireita = getProfundidade(raiz->direita);

	if (tempEsquerda > tempDireita)
		maior = maior + tempEsquerda;
	else
		maior = maior + tempDireita;

	return maior;
}

void Espacos(int n) {
	int i = 0;
	for (; i < n; i++)
		printf(" ");
}


//Função principal. Para imprimir basta chamar ela
void printTree(NoT** raiz) {
	// Profundidade/altura da arvore 
	int profundidade = getProfundidade(*raiz);

	//Espacos é o num de caracteres (espaços e dígitos) máximo de um nível de uma árvore
	//Ou seja, o último nível terá esse comprimento, da folha mais a esquerda
	//até a folha mais a direita.
	//É um número arbitrário e talvez a tela do seu computador
	// comporte mais ou menos espaços
	//Por ex, meu prompt de comando em tela cheia
	//Comporta no máximo 160 espaços/caracteres
	//Coloquei uma potencia de 2 para facilitar	
	int espacos = 128;

	//É o espaço entre os nos no último nível da árvore
	int menorVao = (espacos / (pow(2, profundidade)));

	//valores usados para percorrer a árvore
	//iProf percorre ela na altura: seu valor corresponde ao nivel atual
	//iLargura percorre ela em largura: seu 
	//valor corresponde ao nó atual na largura do nível
	int iProf = 0, iLargura;

	for (int i = 0; i < espacos; i++)
		printf("_");

	//Isso printa a primeira linha, a da raiz

	printf("\n");
	int temp = pow(2, (profundidade - 1 - iProf))*menorVao;
	Espacos(temp);
	printf("%d", (*raiz)->valor);
	Espacos(temp);

	//Um array de pointers (ainda que aqui só tenha um) para nós.
	//É o array com os nós do nivel anterior ao que será impresso
	//a cada iteração do primeiro for abaixo
	NoT** nivelAnt = raiz;

	for (iProf = 1; iProf < profundidade; iProf++) {
		NoT** nivel = (NoT**)malloc(sizeof(NoT*)*(pow(2, iProf)));

		//Este for passa os ponteiros dos nós do nivel a ser impresso agora
		//Usando os ponteiros dos nós do nivel acima. 
		int i;
		for (i = 0; i < (pow(2, iProf - 1)); i++) {

			if (nivelAnt[i] == NULL) {
				nivel[i * 2] = NULL;
				nivel[(i * 2 + 1)] = NULL;
				continue;
			}
			if ((nivelAnt[i]->esquerda) != NULL)
				nivel[i * 2] = nivelAnt[i]->esquerda;
			else
				nivel[i * 2] = NULL;
			if ((nivelAnt[i]->direita) != NULL)
				nivel[(i * 2 + 1)] = nivelAnt[i]->direita;
			else
				nivel[(i * 2 + 1)] = NULL;

		}



		//pulando linhas, adicione linhas para aumentar o espaço
		//entre os níveis
		printf("\n\n\n\n");

		for (iLargura = 0; iLargura < (pow(2, iProf)); iLargura++) {
			//Espaço anterior ao valor
			int espacoAntesEDepoisNesseNivel = pow(2, (profundidade - 1 - iProf))*menorVao;
			//Espaço anterior retificado para centralizar o número com base
			//no número de digitos
			if ((nivel[iLargura]) != NULL) {
				if (nivel[iLargura]->valor > 9)
					espacoAntesEDepoisNesseNivel = espacoAntesEDepoisNesseNivel - 1;
				if (nivel[iLargura]->valor > 99)
					espacoAntesEDepoisNesseNivel = espacoAntesEDepoisNesseNivel - 1;
			}
			Espacos(espacoAntesEDepoisNesseNivel);

			//valor impresso caso haja um nó 
			if ((nivel[iLargura]) == NULL)
				printf("");
			else
				printf("%d", (nivel[iLargura]->valor));

			//espaço posterior 
			espacoAntesEDepoisNesseNivel = pow(2, (profundidade - 1 - iProf))*menorVao;
			//Espaço posterior retificado para centralizar o número com base
			//no número de digitos
			if ((nivel[iLargura]) != NULL) {
				espacoAntesEDepoisNesseNivel = espacoAntesEDepoisNesseNivel - 1;

				if (nivel[iLargura]->valor > 999)
					espacoAntesEDepoisNesseNivel = espacoAntesEDepoisNesseNivel - 1;
			}

			Espacos(espacoAntesEDepoisNesseNivel);
		}

		nivelAnt = nivel;
	}

	free(nivelAnt);


	printf("\n\n\n");
}

//Usa essa função caso queira aumentar a largura em caracteres na tela da sua 
//árvore. Ou caso queira diminuir por sua tela ser pequena (e a árvore sair bagunçada).
void testeEspacos(int n) {


	for (int i = 50; i <= n; i++) {
		printf("<");
		for (int j = 0; j < i - 2; j++) {
			printf(" ");
		}
		printf(">\n\nEspacos acima:%d\nCaso o '>' tenha ficado na linha debaixo de '<', esse numero de caracteres eh muito grande para sua tela.\n\n", i);
	}

}

void Testes() {
	//PILHA
	Pilha* p = criarPilha(15);

	//INSERÇÃO
	push(p, 1);
	push(p, 8);
	push(p, 4);
	push(p, 10);
	push(p, 7);
	push(p, 6);
	push(p, 7);
	push(p, 3);
	push(p, 2);
	push(p, 9);
	printPilha(p);
	//EXCLUSÃO
	pop(p);
	printPilha(p);
	push(p, 9);
	deletarPilha(p, 1);
	printPilha(p);
	push(p, 1);
	//BUSCA
	buscaPilha(p, 20);

	//MAIOR
	maiorPilha(p);
	//MENOR
	menorPilha(p);

	//PILHA CRESCENTE
	Pilha* pcrescente = criarPilha(15);
	//INSERÇÃO
	inserirPilhaCrescente(pcrescente, 4);
	inserirPilhaCrescente(pcrescente, 7);
	inserirPilhaCrescente(pcrescente, 9);
	inserirPilhaCrescente(pcrescente, 1);
	inserirPilhaCrescente(pcrescente, 6);
	inserirPilhaCrescente(pcrescente, 3);
	inserirPilhaCrescente(pcrescente, 2);
	inserirPilhaCrescente(pcrescente, 10);
	inserirPilhaCrescente(pcrescente, 8);
	inserirPilhaCrescente(pcrescente, 5);
	printPilha(pcrescente);
	//EXCLUSÃO
	deletarPilha(pcrescente, 1);
	printPilha(pcrescente);
	inserirPilhaCrescente(pcrescente, 1);
	//BUSCA
	buscaPilhaCrescente(pcrescente, 0);
	//MAIOR
	maiorPilhaCrescente(pcrescente);
	//MENOR
	menorPilhaCrescente(pcrescente);
	inserirPilhaCrescente(pcrescente, 0);

	//PILHA DECRESCENTE
	Pilha* pdecresc = criarPilha(15);
	//INSERÇÃO
	inserirPilhaDecrescente(pdecresc, 4);
	inserirPilhaDecrescente(pdecresc, 7);
	inserirPilhaDecrescente(pdecresc, 1);
	inserirPilhaDecrescente(pdecresc, 6);
	inserirPilhaDecrescente(pdecresc, 9);
	inserirPilhaDecrescente(pdecresc, 3);
	inserirPilhaDecrescente(pdecresc, 2);
	inserirPilhaDecrescente(pdecresc, 10);
	inserirPilhaDecrescente(pdecresc, 8);
	inserirPilhaDecrescente(pdecresc, 5);
	printPilha(pdecresc);
	//EXCLUSÃO
	deletarPilha(pdecresc, 10);
	printPilha(pdecresc);
	inserirPilhaDecrescente(pdecresc, 10);
	//BUSCA
	buscaPilhaDecrescente(pdecresc, 20);
	//MAIOR
	maiorPilhaDecrescente(pdecresc);
	//MENOR
	menorPilhaDecrescente(pdecresc);
	inserirPilhaDecrescente(pdecresc, 11);
	//////////////////

		//FILA
	Fila* fi = criarFila(15);
	//INSERÇÃO
	enfileirar(fi, 5);
	enfileirar(fi, 10);
	enfileirar(fi, 3);
	enfileirar(fi, 2);
	enfileirar(fi, 4);
	enfileirar(fi, 9);
	enfileirar(fi, 1);
	enfileirar(fi, 7);
	enfileirar(fi, 8);
	enfileirar(fi, 6);
	printFila(fi);
	//EXCLUSÃO
	deletarFila(fi, 6);
	printFila(fi);
	enfileirar(fi, 6);
	//BUSCA
	buscarFila(fi, 20);
	//MAIOR
	maiorFila(fi);
	//MENOR
	menorFila(fi);
	enfileirar(fi, 6);
	//FILA CRESCENTE
	Fila* fic = criarFila(15);
	//INSERÇÃO
	inserirFilaCrescente(fic, 5);
	inserirFilaCrescente(fic, 3);
	inserirFilaCrescente(fic, 2);
	inserirFilaCrescente(fic, 4);
	inserirFilaCrescente(fic, 9);
	inserirFilaCrescente(fic, 1);
	inserirFilaCrescente(fic, 7);
	inserirFilaCrescente(fic, 10);
	inserirFilaCrescente(fic, 8);
	inserirFilaCrescente(fic, 6);
	printFila(fic);
	//EXCLUSÃO

	deletarFila(fic, 8);
	printFila(fic);
	inserirFilaCrescente(fic, 6);

	//BUSCA
	buscarFila(fic, 20);

	//MAIOR
	maiorFilaCrescente(fic);
	//MENOR
	menorFilaCrescente(fic);
	inserirFilaCrescente(fic, 11);
	//////////////////

		//LISTA DUPLAMENTE LIGADA 
	No* inicio = criarNo(5);
	//INSERÇÃO
	inserirListaFim(&inicio, criarNo(9));
	inserirListaFim(&inicio, criarNo(6));
	inserirListaFim(&inicio, criarNo(1));
	inserirListaFim(&inicio, criarNo(3));
	inserirListaFim(&inicio, criarNo(7));
	inserirListaFim(&inicio, criarNo(10));
	inserirListaFim(&inicio, criarNo(2));
	inserirListaFim(&inicio, criarNo(8));
	inserirListaFim(&inicio, criarNo(4));
	printLista(inicio);
	//EXCLUSÃO
	removerNo(inicio, buscarValorLista(inicio, 4));
	printLista(inicio);
	inserirListaFim(&inicio, criarNo(4));
	//BUSCA
	buscarValorLista(inicio, 20);
	//MAIOR
	maiorLista(inicio);
	//MENOR
	menorLista(inicio);
	inserirListaFim(&inicio, criarNo(4));
	//LISTA DUPLAMENTE LIGADA CRESCENTE

	No* inicioc = criarNo(6);
	//INSERÇÃO
	inserirListaCrescente(&inicioc, criarNo(5));
	inserirListaCrescente(&inicioc, criarNo(2));
	inserirListaCrescente(&inicioc, criarNo(4));
	inserirListaCrescente(&inicioc, criarNo(9));
	inserirListaCrescente(&inicioc, criarNo(1));
	inserirListaCrescente(&inicioc, criarNo(10));
	inserirListaCrescente(&inicioc, criarNo(8));
	inserirListaCrescente(&inicioc, criarNo(7));
	inserirListaCrescente(&inicioc, criarNo(3));
	printLista(inicioc);

	//EXCLUSÃO
	removerNo(inicioc, buscarValorListaCrescente(inicioc, 10));
	printLista(inicioc);
	inserirListaCrescente(&inicioc, criarNo(10));

	//BUSCA
	buscarValorLista(inicio, 11);
	//MAIOR
	maiorListaCrescente(inicioc);
	//MENOR
	menorListaCrescente(inicioc);
	inserirListaCrescente(&inicioc, criarNo(11));
	//////////////////

		//LISTA DUPLAMENTE LIGADA COM SENTINELA
	sentinela* sent = criarSentinela();
	//INSERÇÃO
	inserirListaFimSent(&sent, criarNo(2));
	inserirListaFimSent(&sent, criarNo(3));
	inserirListaFimSent(&sent, criarNo(8));
	inserirListaFimSent(&sent, criarNo(10));
	inserirListaFimSent(&sent, criarNo(5));
	inserirListaFimSent(&sent, criarNo(1));
	inserirListaFimSent(&sent, criarNo(6));
	inserirListaFimSent(&sent, criarNo(9));
	inserirListaFimSent(&sent, criarNo(7));
	inserirListaFimSent(&sent, criarNo(8));
	printListaSent(&sent);
	//EXCLUSÃO
	removerNoSent(&sent, buscarValorListaSent(&sent, 8));
	printListaSent(&sent);
	inserirListaFimSent(&sent, criarNo(8));
	//BUSCA
	buscarValorListaSent(&sent, 20);
	//MAIOR
	maiorListaSent(&sent);
	//MENOR
	menorListaSent(&sent);
	inserirListaFimSent(&sent, criarNo(8));
	//LISTA DUPLAMENTE LIGADA COM SENTINELA CRESCENTE
	sentinela *sentc = criarSentinela();
	//INSERÇÃO
	inserirListaCrescenteSent(&sentc, criarNo(2));
	inserirListaCrescenteSent(&sentc, criarNo(3));
	inserirListaCrescenteSent(&sentc, criarNo(4));
	inserirListaCrescenteSent(&sentc, criarNo(9));
	inserirListaCrescenteSent(&sentc, criarNo(1));
	inserirListaCrescenteSent(&sentc, criarNo(5));
	inserirListaCrescenteSent(&sentc, criarNo(10));
	inserirListaCrescenteSent(&sentc, criarNo(7));
	inserirListaCrescenteSent(&sentc, criarNo(6));
	inserirListaCrescenteSent(&sentc, criarNo(8));
	printListaSent(&sentc);
	//EXCLUSÃO
	removerNoSent(&sentc, buscarValorListaCrescenteSent(&sentc, 8));
	printListaSent(&sentc);
	inserirListaCrescenteSent(&sentc, criarNo(8));

	//BUSCA
	buscarValorListaCrescenteSent(&sentc, 11);
	//MAIOR
	maiorListaCrescenteSent(&sentc);
	//MENOR
	menorListaCrescenteSent(&sentc);
	inserirListaCrescenteSent(&sentc, criarNo(11));
	//////////////////

		//ARVORE BINARIA BALANCEADA
	NoT* raiz = criarNoT(1);
	//INSERÇÃO
	inserirNoTree(raiz, criarNoT(2));
	inserirNoTree(raiz, criarNoT(3));
	inserirNoTree(raiz, criarNoT(4));
	inserirNoTree(raiz, criarNoT(5));
	inserirNoTree(raiz, criarNoT(6));
	inserirNoTree(raiz, criarNoT(7));
	inserirNoTree(raiz, criarNoT(8));
	inserirNoTree(raiz, criarNoT(9));
	inserirNoTree(raiz, criarNoT(10));
	printTree(&raiz);
	//EXCLUSÃO
	NoT* delet = (buscarTree(raiz, 10));
	deletarTree(&raiz, &delet);
	printTree(&raiz);
	inserirNoTree(raiz, criarNoT(10));
	//BUSCA
	printf("Busca completa %d", buscarTree(raiz, 11));
	//MAIOR
	maiorTree(raiz);
	//MENOR
	menorTree(raiz);
	inserirNoTree(raiz, criarNoT(11));
	//ARVORE BINARIA DESBALANCEADA

	NoT* raizd = criarNoT(1);
	//INSERÇÃO
	raizd->direita = criarNoT(2);
	raizd->esquerda = criarNoT(3);
	raizd->direita->direita = criarNoT(4);
	raizd->esquerda->direita = criarNoT(5);
	raizd->esquerda->direita->esquerda = criarNoT(6);
	raizd->direita->esquerda = criarNoT(7);
	raizd->direita->direita->esquerda = criarNoT(8);
	raizd->direita->direita->direita = criarNoT(9);
	raizd->direita->direita->esquerda->direita = criarNoT(10);

	printTree(&raizd);
	//EXCLUSÃO
	NoT* excluir = (buscarTree(raizd, 10));
	deletarTree(&raizd, &excluir);
	printTree(&raizd);
	raizd->direita->direita->esquerda->direita = criarNoT(10);
	printTree(&raizd);
	//BUSCA
	printf("Busca completa %d", buscarTree(raizd, 11));

	//MAIOR
	maiorTree(raizd);
	//MENOR
	menorTree(raizd);
	raizd->direita->direita->esquerda->direita->esquerda = criarNoT(11);
}

int main() {

	Testes();

	system("pause");
}

