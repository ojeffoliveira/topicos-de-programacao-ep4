==================

Este projeto foi o quarto exercício programa desenvovlido 
para o curso de verão Tópicos de Programação 2019 no IME USP. 
Professora responsável Patrícia Alves.

==================

  O projeto inclui funções que desenvolvi para efetuar a 
impressão de árvores binária de forma visualmente
correta. Estão em \Topicos de programacao ep4\ImprimirArvore.c 

Dessa forma o trabalho do ep em si (desenvolver funções
para inserção, exclusão, busca e maior e menor elemento)
era facilitado pela visualização rápida do resultado das 
funções sem precisar debuggar o programa para checar se os
ponteiros estavam corretos. 

Caso tenha problemas para imprimir as árvores binárias 
leia os comentários no arquivo ou entrem em contato via
ojeffoliveira no facebook e no twitter ou no gitlab. 